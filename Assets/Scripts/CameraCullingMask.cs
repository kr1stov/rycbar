﻿using UnityEngine;
using System.Collections;

public class CameraCullingMask : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void AddCullingMask(int layer)
    {
        GetComponent<Camera>().cullingMask = 1 << layer;
    }

    void DeleteCullingMask(int layer)
    {
        GetComponent<Camera>().cullingMask = ~(1 << layer);
    }




}
