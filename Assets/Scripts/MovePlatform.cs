﻿using UnityEngine;
using System.Collections;

public class MovePlatform : MonoBehaviour {

    public float speed = 1;
    // Use this for initialization
	void Start () {
        GetComponentInChildren<Animator>().speed = speed;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
