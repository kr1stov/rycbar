﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour {
    
    public void SetChildActive(string child, bool state)
    {
        transform.FindChild(child).gameObject.SetActive(state);
    }

    public void SetChildValue(string child, int value)
    {
        transform.FindChild(child).GetComponent<GUIText>().text = value.ToString();
    }

    public void SetChildValue(string child, string value)
    {
        transform.FindChild(child).GetComponent<GUIText>().text = value;
    }
}
