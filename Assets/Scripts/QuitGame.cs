﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {


    private Hud hudManager;

    public int quitCounter;
    // Use this for initialization
    void Start () {
        quitCounter = 0;
        hudManager = GetComponent<Hud>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Cancel"))
        {
            quitCounter++;
            hudManager.SetChildValue("Quit", "press esc again to quit game\npress jump to resume");
        }

        if (quitCounter >= 2)
        {
            Application.Quit();
        }
        else if (quitCounter > 0)
        {
            if (Input.GetButtonDown("Jump"))
            {
                hudManager.SetChildValue("Quit", "");
            }
        }
    }
}
