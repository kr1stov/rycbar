﻿using UnityEngine;
using System.Collections;

public class ControlCamera : MonoBehaviour {

    public float startFollowX;
    public float stopFollowX;

    public bool followModeOn;

    private Transform player;
    
    // Use this for initialization
	void Start () {
        followModeOn = false;
        //GetComponent<ControlCamera>().enabled = false;

        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {

        if (followModeOn == false && player.position.x >= startFollowX)
        {
            followModeOn = true;
        }

        if( followModeOn == true && player.position.x >= stopFollowX)
        {
            followModeOn = false;
        }

        if(followModeOn == true)
        {
            GetComponent<FollowCam>().enabled = true;
            GetComponent<FollowCam>().target = GameObject.FindGameObjectWithTag("Player").transform;
        }
        else
        {
            GetComponent<FollowCam>().enabled = false;
        }
	}
}
