﻿using UnityEngine;
using System.Collections;

public class FollowCamX : MonoBehaviour {

    public int minX;
    public int maxX;
    private Transform player;		// Reference to the player's transform.

    // Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
        if(transform.position.x >= minX && transform.position.x <= maxX)
        { 
            if(player.position.x >= transform.position.x)
            {
                transform.position = new Vector3(player.position.x, transform.position.y, transform.position.z);
            }
        }
        //transform.position = new Vector3(player.position.x, transform.position.y, transform.position.z);
	}
}
