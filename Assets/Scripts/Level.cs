﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

    private GameObject[] Assets { get; set; }
    private string Name { get; set; }
    
    // Use this for initialization
    void Start () {
        Assets = GameObject.FindGameObjectsWithTag("Assets");
        Name = Application.loadedLevelName;
	}
}
