﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {

	float intveral;

	private GUIText titleText;
	private string titleTextText;

    private GUIText attemptsText;
    private string attemptsTextText;

	[SerializeField]
    private int moduloFactor = 2;
	
    public char replaceSymbol = ' ';

	string tempString = "";
	char tempChar = ' ';
	
	string[] splitStrings;

    private Transform loadingText;

    void Awake()
    {
        if (Application.loadedLevelName == "Start")
        {
            loadingText = transform.Find("Loading");
            loadingText.gameObject.SetActive(false);
        }
    }
    // Use this for initialization
    void Start()
    {
        titleText = transform.Find("Status").GetComponent<GUIText>();
        titleTextText = titleText.text;

        splitStrings = titleTextText.Split(' ');

        //InvokeRepeating("RemoveLetters", 0, 2);
        if (Application.loadedLevelName == "Start")
        {
            loadingText = transform.Find("Loading");
        }
        else if (Application.loadedLevelName == "GameOver")
        {
            attemptsText = transform.Find("Attemps").GetComponent<GUIText>();
            attemptsText.text = "you completed the game in " + TheCount.attempts + " attempts.";
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Submit"))
        {
            if (Application.loadedLevelName == "Game Over")
            {
                Application.LoadLevel(0);
            }
            else if (Application.loadedLevelName == "Start")
            {
                RemoveLetters();
            }
        }
        else if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }

	void RemoveLetters()
	{
		for(int j=0; j<splitStrings.Length; j++)
		{
			tempString = "";
			for(int i=0; i<splitStrings[j].Length; i++)
			{
				tempChar = splitStrings[j][i];

				if(i > 0)
                {
                    if (moduloFactor > 0)
                    {
                        if (i % moduloFactor == 0)
                        {
                            tempChar = replaceSymbol;
                        }
                    }
                    else
                    {
                        loadingText.gameObject.SetActive(true);
                        Application.LoadLevel(Application.loadedLevel + 1);
                    }
				}
				tempString += tempChar;
			}
			splitStrings[j] = tempString;
		}

		titleText.text = "";
		for(int j=0; j<splitStrings.Length; j++)
		{
			titleText.text += (splitStrings[j] + " ");
		}
		moduloFactor--;
	}

}
