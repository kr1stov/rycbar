﻿using UnityEngine;
using System.Collections;

public class AutoAssignLayer : MonoBehaviour {

    [SerializeField]
    private int width = 2;
    private int startLayer = 10;

    // Use this for initialization
	void Start () {
        gameObject.layer = startLayer + ((int) transform.position.x / width);
        //GameMaster.numberOfLayers++;

        for(int i=0; i< transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.layer = gameObject.layer;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
