﻿using UnityEngine;
using System.Collections;

public class EndOfLevel : MonoBehaviour {

    private LayerMask ignorLayers;
    private enum HideMode { Single, Multiple };

    [SerializeField]
    private HideMode hideMode;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (tag == "Exit" && other.tag == "Player")
        {
            if (GameMaster.stagesLeft == 0)
            {
                Application.LoadLevel(Application.loadedLevel + 1);
            }
            else
            {
                GameMaster.RespawnPlayer();

                if (hideMode == HideMode.Multiple)
                {
                    for (int i = 1; i <= 8; i++)
                    {
                        if (i % GameMaster.stagesLeft == 0)
                        {
                            ignorLayers |= (1 << (10 + i));
                            Camera.main.cullingMask = ~ignorLayers;
                        }
                    }
                }
                else if (hideMode == HideMode.Single)
                {
                    ignorLayers |= (1 << (10 + GameMaster.stagesLeft));
                    Camera.main.cullingMask = ~ignorLayers;
                }

                GameMaster.stagesLeft--;
            }
        }
    }
}
