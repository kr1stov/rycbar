﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {

	
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameMaster.RespawnPlayer();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {   
        if (other.gameObject.tag == "Player")
        {
            GameMaster.RespawnPlayer();
        }
    }
}
