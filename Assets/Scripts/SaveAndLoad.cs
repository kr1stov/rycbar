﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class SaveAndLoad : MonoBehaviour {

    private Level currentLevel;
    // Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.F6))
        {
            Save("testSave.xml");
        }
	}

    void Save(string fileName)
    {
        currentLevel = GameObject.Find("Level").GetComponent<Level>();

        XmlSerializer serializer = new XmlSerializer(typeof(Level));
        using (TextWriter writer = new StreamWriter(@"C:\testLevel.xml"))
        {
            serializer.Serialize(writer, currentLevel);
        }
    }
}
