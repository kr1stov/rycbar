﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour
{

    public static int attemps;

    //public static int level;
    public static string level;
    public static int stagesLeft;
    public static int numberOfStages;
    public static int numberOfLayers = 0;

    [SerializeField]
    private static GameObject player;

    [SerializeField]
    private static GameObject startPoint;


    public int stages;

    /*public static int Level
    {
        set { level = value; }
        get { return level; }
    }

    public static int Attemps
    {
        set { attemps = value; }
        get { return attemps; }
    }*/



    private Hud hudManager;
    //private TheCount theCount;

    // Use this for initialization
    void Start()
    {
        attemps = 0;
        //level = 0;
        level = Application.loadedLevelName;
        numberOfStages = stages;
        stagesLeft = numberOfStages;

        //numberOfAssets = GameObject.FindGameObjectsWithTag("Assets").Length;

        //theCount = GetComponent<TheCount>();

        hudManager = GameObject.Find("HUD_Game").GetComponent<Hud>();

        hudManager.SetChildValue("Attemps/Counter", attemps);
        hudManager.SetChildValue("Level/Counter", level);
        hudManager.SetChildValue("Laps/Counter", stagesLeft);

        player = GameObject.FindGameObjectWithTag("Player");
        startPoint = GameObject.FindGameObjectWithTag("Respawn");

    }

    // Update is called once per frame
    void Update()
    {
        hudManager.SetChildValue("Attemps/Counter", attemps);

        //level = Application.loadedLevel;


        //hudManager.SetChildValue("Level/Counter", level);

        hudManager.SetChildValue("Laps/Counter", stagesLeft);

        if (Input.GetButton("ChangeDown") && Input.GetButton("Cancel"))
        {
            Application.LoadLevel(0);
        }
        else if (Input.GetButtonDown("ChangeUp"))
        {
            ChangeLevel(1);
        }
        else if (Input.GetButtonDown("ChangeDown"))
        {
            ChangeLevel(-1);
        }
    }

    public static void RespawnPlayer()
    {
        player.transform.position = startPoint.transform.position;
        attemps++;
        TheCount.attempts++;
    }


    public void ChangeLevel(int val)
    {
        if(val >= 0)
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel - 1);
        }
    }


}
