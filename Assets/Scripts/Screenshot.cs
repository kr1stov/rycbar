﻿using UnityEngine;
using System.Collections;

public class Screenshot : MonoBehaviour {

    public string folderName;
    public float interval;

    private float nextTime;
    private string fileName;
    private string levelName;
    // Use this for initialization
	void Start () {
        Capture(MakeFileName(Application.loadedLevelName, Time.timeSinceLevelLoad));

        nextTime = Time.timeSinceLevelLoad + interval;
	}
	
	// Update is called once per frame
	void Update () {
	    //if(Time.timeSinceLevelLoad >= nextTime)
        //{
            Capture(MakeFileName(Application.loadedLevelName, Time.timeSinceLevelLoad));
            //nextTime = Time.timeSinceLevelLoad + interval;
        //}
	}

    void Capture(string fileName)
    {

        Application.CaptureScreenshot(folderName + "/" + fileName);
    }

    string MakeFileName(string level, float time)
    {
        return (level + "_" + Mathf.FloorToInt(time * 1000).ToString() + ".png");
    }
}
